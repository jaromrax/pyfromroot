#!/usr/bin/env python3
"""
These should be different general tools for gregory online
a pyfromroot
....
JUST A BUNCH NOW
"""

from fire import Fire
import numpy as np
import matplotlib.pyplot as plt
import scipy
import scipy.ndimage


# _------------------------  REBIN WITH IMAGE PROCESSING (better version)

def rebin_np( spectrum , sca, shi, order = 3):
    """
    should do sca* spectrum + shi .... order 3 has sometimes lower errors
    """
    spectrumb = scipy.ndimage.zoom(spectrum, sca, order=order)
    spectrumb = scipy.ndimage.interpolation.shift(spectrumb, shi , cval=0, order = order)
    asu = spectrum.sum()
    bsu = spectrumb.sum()
    spectrumb=spectrumb/bsu*asu
    df = len(spectrumb) - len(spectrum)
    #print( f" b={len(b)}    a={len(a)}   df = {df}")
    while df<0:
        spectrumb = np.append( spectrumb,[0] )
        df = len(spectrumb) - len(spectrum)
    if df>0:
        spectrumb = spectrumb[:-df]
    #print( len(a),"X" ,len(b),  f" 6 -->  {6*sca+shi}" )
    #print( len(a),"X" ,len(b),  f" 22 -->  {22*sca+shi}" )
    return spectrumb






def th2np( h ):
    nbins=h.GetNbinsX()
    nh = np.zeros( nbins)
    for i in range(1,nbins+1):
        nh[i-1] = h.GetBinContent(i)
    return nh



def np2th( n ):
    nbins=len(n)
    th = ROOT.TH1D("test","test", nbins,0,nbins)
    for i in range(1,nbins+1):
        if n[i-1]>=0:
            th.SetBinContent( i, n[i-1] )
        else:
            th.SetBinContent( i, 0)
    return th



#***************************************************************
def main():
    print()

if __name__=="__main__":
    Fire(main)
