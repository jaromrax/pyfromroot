PyFromRoot - fitting with iminuit in root
=========================================

The goal is to have a framework to combine Python and iminuit with root
to get the best of both.

Basic commands for the moment

-   `load` - load csv like data or histogram
-   `zoom` - zoom to the desired range
-   `fit` - fit with a desired model

Fit models - filename is like `pr_model_*.py` and it will be recognized
by the loader

-   `polall` - model for 0-5th degree polynomials
-   `pocall` - model for 0-5th degree chebyshev polymonials
-   `logxy` - model for pol in log space
-   `gpol1` - model for gauss plus pol1
-   `decay` - model for exponential
-   `circle` - model for circle - image 640x480, center at top

Features and issues
===================

Issues
------

-   2207 - out{position}.tab in fitpy ... was JUST for efficiency

Features
--------

-   2207
    -   if `#COLNAME:` is missing AND the 1st line contains less than 2
        float fields, it is taken as a header
-   2205 and older
    -   `#COLNAME:`
    -   `#LOAD_AS:`
        -   LOADAS can contain `cala=1.0` and `calb=0.0` to define the
            calibration for histogram
    -   TGraph or TH1istogram can be loaded, depending on the argument
        ([,]{.underline},x,y) or [,]{.underline},h
    -   MODEL EDIT without any compiilation (model modules are
        unloaded/loaded every run)

Example data files:
===================

Automatic generated file, no header. % will probably crash

``` {.example}
ch  start   Treal   Ntot    rate    Twin    Nsat    Tsat    min_dtus    Npu Tblind  bliER%  DtSBl%  DtSBlE% Z   Zd  Zs  Zi  Zsta    DT% Tlive   Tdead   Nzero   sum_dsi
0   0   60.0    5117568 85292.82    3.5 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 700682  4   2   431531  49.47   30.32   29.68   2531822 700688
0   0   60.0    5117568 85292.82    3.6 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 708596  4   2   377985  49.47   30.32   29.68   2531822 708602
0   0   60.0    5117568 85292.82    3.7 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 714688  4   2   330343  49.47   30.32   29.68   2531822 714694
0   0   60.0    5117568 85292.82    3.8 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 722503  4   2   273311  49.47   30.32   29.68   2531822 722509
0   0   60.0    5117568 85292.82    3.9 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 727277  4   2   227680  49.47   30.32   29.68   2531822 727283
0   0   60.0    5117568 85292.82    4.0 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 733274  4   2   172413  49.47   30.32   29.68   2531822 733280
0   0   60.0    5117568 85292.82    4.1 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 737982  4   3   123531  49.47   30.32   29.68   2531822 737989
0   0   60.0    5117568 85292.82    4.2 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 742174  5   3   75251   49.47   30.32   29.68   2531822 742182
0   0   60.0    5117568 85292.82    4.3 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 738496  9251    3   41548   49.47   30.32   29.68   2531822 747750
0   0   60.0    5117568 85292.82    4.4 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 725276  32220   3   18165   49.47   30.32   29.68   2531822 757499
0   0   60.0    5117568 85292.82    4.42    17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 722869  36871   3   13538   49.47   30.32   29.68   2531822 759743
0   0   60.0    5117568 85292.82    4.44    17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 720086  41244   3   9089    49.47   30.32   29.68   2531822 761333
0   0   60.0    5117568 85292.82    4.46    17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 717629  45881   3   4477    49.47   30.32   29.68   2531822 763513
0   0   60.0    5117568 85292.82    4.48    17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 715031  50429   3   0   49.47   30.32   29.68   2531822 765463
0   0   60.0    5117568 85292.82    4.5 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 712654  52320   2069    0   49.47   30.32   29.68   2531822 767043
0   0   60.0    5117568 85292.82    4.6 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 704518  58676   8767    0   49.47   30.32   29.68   2531822 771961
0   0   60.0    5117568 85292.82    4.7 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 696418  64940   15418   0   49.47   30.32   29.68   2531822 776776
0   0   60.0    5117568 85292.82    4.8 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 689229  70623   21370   0   49.47   30.32   29.68   2531822 781222
0   0   60.0    5117568 85292.82    4.9 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 681262  76913   28040   0   49.47   30.32   29.68   2531822 786215
0   0   60.0    5117568 85292.82    5.0 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 672845  83782   35292   0   49.47   30.32   29.68   2531822 791919
0   0   60.0    5117568 85292.82    5.5 17  0.0 0.43    16  2.2 3.6 53.15   53.08   2531822 635930  113460  67336   0   49.47   30.32   29.68   2531822 816726

```

``` {.example}
#COLNAMES: e,de,i,res
# 3  3  200  50  300  100 3000
#
#-1.05226634682401E+0002
# 6.01109363525447E+0001
#-1.16256065205872E+0001
# 7.35461175272135E-0001
#
# 1.53818522163346E+0001
#-7.87900615552167E+0000
# 1.10201006049770E+0000
#-5.58118882800294E-0002
#
   43.4  0.038328  0.011908    221.87
   59.5  0.046189  0.029160     58.40
   79.6  0.034475  0.041850    -17.62
   99.0  0.039296  0.043870    -10.43
  103.0  0.047777  0.043522      9.78
  121.8  0.032901  0.040217    -18.19
  122.1  0.041350  0.040156      2.97
  122.1  0.041362  0.040156      3.00
  136.5  0.040782  0.036832     10.73
  136.5  0.040718  0.036832     10.55
  160.6  0.036582  0.031409     16.47
  223.2  0.019474  0.021806    -10.69
  244.7  0.020412  0.020189      1.10
  276.4  0.017974  0.018313     -1.85
  295.9  0.018107  0.017376      4.21
  302.9  0.016848  0.017075     -1.33
  324.8  0.017575  0.016210      8.42
  329.4  0.013515  0.016044    -15.76
  344.3  0.016613  0.015541      6.90
  356.0  0.014919  0.015173     -1.67
  367.8  0.019788  0.014829     33.44
  383.9  0.016927  0.014393     17.60
  411.1  0.014025  0.013732      2.13
  416.1  0.012081  0.013621    -11.31
  444.0  0.012653  0.013040     -2.97
  488.7  0.011799  0.012242     -3.62
  503.5  0.011493  0.012008     -4.28
  520.2  0.013279  0.011756     12.96
  564.0  0.012402  0.011164     11.09
  566.4  0.011864  0.011134      6.56
  586.3  0.011003  0.010893      1.01
  656.5  0.009605  0.010144     -5.31
  661.7  0.008729  0.010094    -13.52
  661.7  0.010775  0.010094      6.75
  674.7  0.010684  0.009971      7.14
  678.6  0.008681  0.009935    -12.63
  688.7  0.009650  0.009844     -1.97
  692.0  0.011647  0.009814     18.68
  692.0  0.008841  0.009814     -9.91
  712.8  0.009169  0.009634     -4.82
  719.4  0.007469  0.009579    -22.03
  778.9  0.008634  0.009114     -5.26
  810.5  0.009508  0.008889      6.96
  834.9  0.009265  0.008725      6.18
  841.6  0.009957  0.008681     14.70
  867.4  0.007188  0.008517    -15.61
  898.0  0.005755  0.008332    -30.93
  919.3  0.006245  0.008209    -23.93
  926.3  0.005293  0.008170    -35.21
  930.6  0.012639  0.008146     55.15
  964.1  0.006935  0.007964    -12.92
 1085.9  0.007142  0.007375     -3.15
 1089.7  0.006226  0.007357    -15.37
 1113.0  0.006389  0.007256    -11.96
 1173.2  0.006753  0.007008     -3.64
 1213.0  0.005726  0.006853    -16.45
 1249.9  0.007554  0.006716     12.47
 1292.8  0.006537  0.006564     -0.41
 1299.1  0.005964  0.006542     -8.84
 1332.5  0.006119  0.006429     -4.83
 1408.0  0.005330  0.006188    -13.87
 1457.6  0.005740  0.006039     -4.95
 1836.1  0.003250  0.005092    -36.17
```

``` {.example}
#COLNAME: time,c,dc,f,df,e,de,ef,def,area,darea,chi2
   30   1659.93    0.05    5.18    0.11     1123.69    0.03    3.50    0.07         50.86         1.10     0.86
   90   1659.87    0.07    5.33    0.16     1123.65    0.05    3.61    0.11         34.68         1.05     1.22
  150   1659.72    0.12    5.24    0.27     1123.55    0.08    3.55    0.18         19.99         1.03     1.77
  210   1659.73    0.15    5.17    0.34     1123.56    0.10    3.49    0.23         11.87         0.81     1.54
  270   1659.41    0.17    5.28    0.43     1123.34    0.12    3.57    0.29          7.40         0.57     0.58
#  330   1659.45    0.22    5.66    0.52     1123.37    0.15    3.83    0.35          5.94         0.55     0.92
#  390   1659.94    0.37    4.72    0.87     1123.69    0.25    3.19    0.59          2.46         0.45     1.12
#  450   1660.00    0.76    7.56    0.12     1123.73    0.52    5.11    0.08          2.12         0.43     1.02
#  510   1659.27    0.36    2.91    0.86     1123.24    0.25    1.97    0.58          1.00         0.28     0.87
#  570   1661.82    1.72    6.83    2.66     1124.97    1.17    4.62    1.80          1.05         0.50     1.37
```

Usage
=====

From root (problems now)
------------------------

For text file with columns

    .L prun.C
    prun("load filename _,x,y")

For text file with one column and histogram

    .L prun.C
    prun("load nn.asc1 h")
    prun("zoom nn 3827,30")

TGraph of name `filename_x_y` is created, kept in
`gROOT->GetSpecials()`, visible with `shspe()`

From python (changes in time now)
---------------------------------

Canvas control 2022/11 . If there is no canvas name in FIT, no display
is done....

``` {.python results="replace output" session="test" exports="both"}
#!/usr/bin/env python3

from fire import Fire
from pyfromroot import prun
import ROOT
import time

def main():

    prun.do("load","n2.asc1 h")
    prun.do("zoom","n2 13825,60") # 14042.88   154
    prun.do("draw","n2", canvas = "cn2")
    res2 = prun.do("fit","n2 gpol1", canvas="xoxo2")
    #return

    prun.do("load","nn.asc1 h")
    prun.do("zoom","nn 6582,29") # 14059.07
    prun.do("draw","nn", canvas = "cnn")
    res1 = prun.do("fit","nn gpol1", canvas="xoxo")
    print("X"*60)

    print( res1.keys() )
    for i in  ['area','darea','channel','chi2dof']: print(f"{i:12s} nn: {res1[i]:10.2f}   n2:{res2[i]:10.2f}")

if __name__=="__main__":
    Fire(main)
    while ROOT.addressof(ROOT.gPad)!=0: time.sleep(0.2)

```

Full example earlier 2022:

``` {.python results="replace output" session="test" exports="both"}
#!/usr/bin/env python3

from  pyfromroot import  prun
import ROOT
import time
import sys
from fire import Fire

def calib():
    # loading
    prun.loadpy("load","calib.txt x,y,dy")

    # fitting
    res = prun.loadpy("fit","calib pol2")  #print(res.keys() )



def gaus():

    # loading histogram
    prun.loadpy("load","cugamma_cu4.txt _,h")


    # zooming
    prun.loadpy("zoom","cugamma_cu4 7432,50")


    # fitting
    res = prun.loadpy("fit","cugamma_cu4 gpol1")  #print(res.keys() )




    if ( type(res) is dict) and ( res['noerror']):
        print(f"@ {res['channel']:.2f} A = {res['area']:.2f} {res['darea']:.2f}")
        print(res['diff_fit_int_proc'],"%" )
    else:
        print("X... problem in fit OR data not returned in dict")



if __name__ == "__main__":
    Fire()
    # wait closing
    while ROOT.addressof(ROOT.gPad)!=0: time.sleep(0.2)
    sys.exit(0)



```

Versions
========

-   0.1.
-   0.2. works on ubuntu 22 with numpy 1.23.5
