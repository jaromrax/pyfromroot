#!/usr/bin/env python3

from  pyfromroot import  prun
import ROOT
import time
import sys
from fire import Fire
import os

def main( filename ):
    # loading
    prun.loadpy("load",f"{filename} q,x,y")

    print("i... FITTING")
    filenamenoext = os.path.splitext(filename)[0]
    print(filenamenoext)

    # fitting
    res = prun.loadpy("fit",f"{filenamenoext} circle")


if __name__ == "__main__":
    Fire(main)
    # wait closing
    while ROOT.addressof(ROOT.gPad)!=0: time.sleep(0.2)
    sys.exit(0)
