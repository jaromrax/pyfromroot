#!/usr/bin/env python3
"""
 A TEST SCRIPT - I need to debug a remake the DRAW + canvas YES/NO
 also fit comparing the rebin results....
"""

from fire import Fire
from pyfromroot import prun
import ROOT
import time

def main():

    prun.do("load","n2.asc1 h")
    prun.do("zoom","n2 13825,60") # 14042.88   154
    prun.do("draw","n2", canvas = "cn", opt="logy", color="red" )

    prun.do("load","nn.asc1 h")
    # FOR SAME - do not use zoom - it will not match with the 1st one
    #prun.do("zoom","nn 6582,29") # 14059.07
    prun.do("draw","nn", canvas = "cn", opt = "same", color="green")

    return

    res2 = prun.do("fit","n2 gpol1")  # NO CANVAS
    #return

    res1 = prun.do("fit","nn gpol1", canvas="xoxo")
    print("X"*60)

    print( res1.keys() )
    for i in  ['area','darea','channel','chi2dof']: print(f"{i:12s} nn: {res1[i]:10.2f}   n2:{res2[i]:10.2f}")

if __name__=="__main__":
    Fire(main)
    print("i... waiting for closing a Canvas..... this will end just after")
    while ROOT.addressof(ROOT.gPad)!=0: time.sleep(0.2)
