#include <iostream>
#include "TPython.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TH1I.h"
#include "TROOT.h"
#include "TText.h"
/*
usage (after compilation):
      prun("test")
result:
      TPython::LoadMacro( "pr_test.py")



To run pPython from C++ ...... I have only
 - Eval
 - Exec
 - Prompt
 - LoadMacro
--------------------------
  I try to use ListOfSpecials and TText to transport contents via /Name and Title/
----------------------------
 at the end I run  LoadMacro( pythonfile )   where the mask of available files if pr_*.py
*/

void prun(){
  printf("H...  use a string to pass the command:\n ... e.g.  help OR load filename%s","\n");
}

void prun(const char* command){

  TText *linp;

  // -----prepare input and output --------------
  cout<<"i... deleting all  pr_input/pr_output TText objects"<<endl;

  linp= (TText*)gROOT->GetListOfSpecials()->FindObject("pr_input");
  while (linp!=NULL){
    //cout<<"i... deleting old pr_input"<<endl;
    linp->SetName("pyremoved1");
    gROOT->GetListOfSpecials()->Remove(linp);
    linp= (TText*)gROOT->GetListOfSpecials()->FindObject("pr_input");
    delete linp;
  }
  linp= (TText*)gROOT->GetListOfSpecials()->FindObject("pr_output");

  while (linp!=NULL){
    //cout<<"i... deleting old pr_output"<<endl;
    linp->SetName("pyremoved2");
    gROOT->GetListOfSpecials()->Remove(linp);
    linp= (TText*)gROOT->GetListOfSpecials()->FindObject("pr_output");
    delete linp;
  }
  //cout<<"i... checking pr_input/pr_output DONE"<<endl;

  ////cout<<"i... listing the content of gRootSpecials"<<endl;
  //for (int i=0;i<gROOT->GetListOfSpecials()->GetSize();i++){
  //  cout<<gROOT->GetListOfSpecials()->At(i)->GetName()<<endl;
  // }


  linp=new TText();
  linp->SetName( "pr_input" );
  linp->SetTitle( command );   // tricky way to deplay a command to ROOT
  gROOT->GetListOfSpecials()->Add(linp);

  cout<<"i... new input for prun.py constructed, running..."<<endl;

  //cout<<"I am in C++ now"<<endl;
  TPython::Exec("import prun");// import - BUT FOREVER during the same session
  TPython::Exec("prun.loadpy()");// import - BUT FOREVER during the same session


  return;  // I dont know how to put in gDirectory to be visible......


  //cout<<"i... listing the content of gRootSpecials"<<endl;
  for (int i=0;i<gROOT->GetListOfSpecials()->GetSize();i++){
    if (
	( strstr( gROOT->GetListOfSpecials()->At(i)->ClassName()  ,"TGraph")!=NULL )||
	( strstr( gROOT->GetListOfSpecials()->At(i)->ClassName()  ,"TH")!=NULL )
	){
      cout<<"adding"<<gROOT->GetListOfSpecials()->At(i)->ClassName() <<endl;
      gDirectory->Add(gROOT->GetListOfSpecials()->At(i));
    }
    cout<<gROOT->GetListOfSpecials()->At(i)->GetName()<<endl;
  }

  return;

}



/* -------------- this is how to get back the resulting string
  //-----------extracting the resulting Title string ----------------
  // //gROOT->GetListOfSpecials()->Print();
  // TText *liout = (TText*)gROOT->GetListOfSpecials()->FindObject("pr_output");
  // if (liout==NULL){
  //   return;
  // }
  // TString *s=new TString( liout->GetTitle() );
  // cout<< "i... returned Title=="<<s->Data()<<endl;
  // //=============================== RUN FILE =======================
  // TPython::LoadMacro( s->Data() );

  //liout->Print();
  //#cout<<"i... back i C++ :I have in Title:"<<liout->GetName()<<endl;
  //#cout<<"i... back i C++ :I have in Title:"<<liout->GetTitle()<<endl;
  */
