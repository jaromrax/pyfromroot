#!/usr/bin/env python3

from fire import Fire

def main():
    print()

#if __name__=="__main__":
#    Fire(main)


from iminuit import Minuit
from iminuit.cost import UnbinnedNLL
from iminuit.util import describe
from matplotlib import pyplot as plt
import numpy as np
from numba_stats import norm
import time


# generate two data sets which are fitted simultaneously
rng = np.random.default_rng(1)

width = 2.0
data1 = rng.normal(0, width, size=1000)
data2 = rng.normal(5, width, size=1000)

# use two pdfs with different names for non-shared parameters,
# so that they are fitted independently

def pdf1(x, μ_1, σ):
    return norm.pdf(x, μ_1, σ)

def pdf2(x, μ_2, σ):
    return norm.pdf(x, μ_2, σ)


# combine two log-likelihood functions by adding them
lh = UnbinnedNLL(data1, pdf1) + UnbinnedNLL(data2, pdf2)

print("LH=")
print(lh) # costsum object
print(UnbinnedNLL(data1, pdf1)) # unbinnedNLL objekt

print(f"{describe(lh)=}")

#  dostane style = cokoliv co je kwargs
#
def plot(cost, xe, minuit, ax, **style):
    print("plot call")
    signature = describe(cost)
    print(signature)
    #data = cost.data

    values = minuit.values[signature]
    errors = minuit.errors[signature]
    print(values)

    cx = (xe[1:] + xe[:-1]) / 2

    ym = np.diff(norm.cdf(xe, *values)) * np.sum(w)
    t = []
    for n, v, e in zip(signature, values, errors):
        t.append(f"${n} = {v:.3f} ± {e:.3f}$")
    ax.plot(cx, ym, label="\n".join(t), **style)

print("fitting")



m = Minuit(lh, μ_1=1, μ_2=2, σ=1)

print("plotting  prep")
fig, ax = plt.subplots(1, 2, figsize=(14, 5))


# vyrobeni seznamu 2 histogramu ...  z dat !
# kazdy histo jsou 2 arraye: values a bins
hists = [np.histogram(lhi.data, bins=50) for lhi in lh]

print(len(hists))
print("histo done")


# draw data and model with initial parameters
#   lh je i model i data
#
for lhi, (w, xe), axi in zip(lh, hists, ax):
    cx = (xe[1:] + xe[:-1]) / 2  # poresi biny, kterych je asi -1
    axi.errorbar(cx, w, np.sqrt(w), fmt="ok", capsize=0, zorder=0)
    # 3. je minuit
    plot(lhi, xe, m, axi, ls=":")

#plt.show()

print("going migrad")


m.migrad()
print(m)


# draw model with fitted parameters
for lhi, (w, xe), axi in zip(lh, hists, ax):
    plot(lhi, xe, m, axi, ls="-", color="red")
    axi.legend()

plt.show()
print("sleeinpg")
time.sleep(5)
